# Trabalho 2 - 2022/2

Trabalho 2 da disciplina de Fundamentos de Sistemas Embarcados (2022/2)

## 1. Objetivos

Este trabalho tem por objetivo a implementação de um sistema (que simula) o controle de um forno para soldagem de placas de circuito impresso (PCBs). Abaixo vemos alguns exemplos de fornos comerciais para este propósito.

Reflow Oven - iTECH RF-A350  | Forno LPKF Protoflow S4  |  Controleo3 Reflow Oven
:-------------------------:|:-------------------------:|:-------------------------:
<img src="https://cdn.shopify.com/s/files/1/0561/1136/6234/products/ReflowOvenMachine7_3063a9f2-2046-4d42-b79f-b0d3c27bc755_700x.jpg?v=1659768134" data-canonical-src="https://cdn.shopify.com/s/files/1/0561/1136/6234/products/ReflowOvenMachine7_3063a9f2-2046-4d42-b79f-b0d3c27bc755_700x.jpg?v=1659768134" width="200"/> | <img src="https://www.lpkf.com/fileadmin/mediafiles/_processed_/1/b/csm_lpkf_protoflow_s4_05300933fa.png" data-canonical-src="https://www.lpkf.com/fileadmin/mediafiles/_processed_/1/b/csm_lpkf_protoflow_s4_05300933fa.png" width="200"/> | <img src="https://www.whizoo.com/i/c3/c3_oven.jpg" data-canonical-src="https://www.whizoo.com/i/c3/c3_oven.jpg" width="150"/>

No trabalho, o aluno deverá desenvolver o software que efetua o controle de temperatura do forno utilizando dois atuadores para este controle: um **resistor de potência** de 15 Watts utilizado para aumentar temperatura e; uma **ventoinha** que puxa o ar externo (temperatura ambiente) para reduzir a temperatura do sistema. 

Os comandos do usuário do sistema para definir a temperatura desejada serão controlados de duas maneiras:
1. Manualmente através do seletor de temperatura no dashboard (Thingsboad);
2. Automaticamente seguindo uma curva de temperatura pré-definida em arquivo de configuração ([Arquivo da Curva](./curva_reflow.csv)).

O controle da temperatura será realizado através da estratégia PID onde deverão estar disponíveis para o usuário o ajuste dos parâmetros Kp, Ki e Kd nas configurações do sistema (Via terminal).

## 2. Controle PID

A abordagem de controle a ser utilizado é o controle PID (Proporcional Integral Derivativo). O PID é um dos tipos mais simples de algoritmos de controle que proporciona um bom desempenho para uma grande variedade de aplicações.

O conceito fundamental desse tipo de controlador se baseia em monitorar uma variável de um processo (neste caso a temperatura interna) e medir a diferença entre seu valor atual (TI - Temperatura Interna) a uma valor de referência (TR - Temperatura de Referência) desejado. A partir dessa medida de **Erro = TR - TI**, toma-se uma ação de correção para que o sistema alcançe o valor desejado. A figura abaixo demonstra uma visão geral da estratégia de controle.

![Sistema de Controle](https://upload.wikimedia.org/wikipedia/commons/2/24/Feedback_loop_with_descriptions.svg)

O controle PID une três tipos de ações sobre a variável de controle para minimizar o erro do sistema até que o mesmo alcançe a referência desejada. No caso deste sistema, nossa variável monitorada é a TI - Temparatura Interna e o seu controle é efetuado através do acionamento da **Resistência (R)** ou da **Ventoinha (V)** e nosso **Erro** é a diferença entre a temperatura de referência e a temperatura interna do sistema (Erro = TR - TI).

Detalhando as 3 ações do PID temos:
- **Controle Proporcional (P)**: ajusta a variável de controle de forma proporcional ao erro, ou seja, quanto maior o erro, maior a intensidade de acionamento do resistor (0 a 100%). Esse ajuste é feito pela variável ***Kp***.
- **Controle Integral (PI)**: ajusta a variável de controle baseando-se no tempo em que o erro acontece, acumulando este erro (integral). Esse ajuste é feito pela variável ***Ki***.
- **Controle Derivativo (PD)**: ajusta a variável de controle tendo como base a taxa de variação do erro ou a velocidade com a qual o sistema está variando o erro. Esse ajuste é feito pela variável ***Kd***.

A figura abaixo mostra as equações envolvidas no cálculo do PID.

![PID](https://upload.wikimedia.org/wikipedia/commons/4/43/PID_en.svg)

O ajustes de cada constante do PID (Kp, Ki e Kd) tem efeitos distintos no controle do sistema conforme pode ser visto na figura  abaixo.

![PID - Variáveis Kp, Ki, Kd](https://upload.wikimedia.org/wikipedia/commons/3/33/PID_Compensation_Animated.gif)


## 4. Componentes do Sistema

O sistema é composto por:
1. Ambiente fechado controlado com o resistor de potência e ventoinha;
2. 01 Sensor DS18B20 (1-Wire) para a medição da temperatura interna (**TI**) do sistema;
3. 01 Sensor BME280 (I2C) para a medição da temperatura externa (**TE**);
4. 01 Conversor lógico bidirecional (3.3V / 5V);
5. 01 Driver de potência para acionamento de duas cargas (L297);
6. 01 ESP32;
7. 01 Raspberry Pi 4;

![Figura](/figuras/Figura_Trabalho_2_ReflowOven.png)

## 5. Conexões entre os módulos do sistema

1. O sensor de temperatura BME280 está ligado ao barramento I2C da Raspberry Pi e utiliza o endereço (0x76);
2. O resistor de potência e a ventoinha estão ambos ligados às portas GPIO e são acionados através do circuito de potência:  
    2.1. Resistor: GPIO 23;  
    2.2. Ventoinha: GPIO 24.
3. A ESP32 está conectada à placa Raspberry Pi via UART (Protocolo MODBUS-RTU);
5. Os comandos de acionamento e controle do sistema virão do Dashboard (Thingsboard) através da ESP32;
6. O sensor de temperatura DS18B20 para medição do ambiente controlado está ligado à ESP32 na porta GPIO 4 via protocolo 1-Wire;

## 6. Requisitos

Os sistema de controle possui os seguintes requisitos:
1. O código deve ser desenvolvido em C/C++ ou Python;
2. Na implementação do software, não podem haver loops infinitos que ocupem 100% da CPU;
3. O sistema deve implementar o controle de temperatura do ambiente fechado utilizando o controle PID atuando sobre o Resistor e a Ventoinha;
4. A interface de terminal do usuário deve prover a capacidade de definição dos seguintes parâmetros:
   1. **Temperatura de referência (TR)**: deve haver uma opção para escolher se o sistema irá considerar TR definido pelo teclado (Modo Debug), pelo valor definido no dashboard via UART ou seguindo as curvas de temperatura pré-definidas em arquivo.
   2. **Parâmetros Kp, Ki, Kd**: para o controle PID deve ser possível definir os valores das constantes Kp, Ki e Kd;
5. No caso da temperatura ser definida via UART, o programa deve consultar o valor através da comunicação UART-MODBUS com a ESP32 a cada 1 segundo;
6. O programa deve gerar um log em arquivo CSV das seguintes informações a cada 01 segundo com os seguintes valores: (Data e hora, temperatura interna, temperatura externa, temperatura definida pelo usuário, valor de acionamento dos atuadores (Resistor e Venoinha em valor percentual)).
7.  O programa deve tratar a interrupção do teclado (Ctrl + C = sinal **SIGINT**) encerrando todas as comunicações com periféricos (UART / I2C / GPIO) e desligar os atuadores (Resistor e Ventoinha);
8. O sistema deve conter em seu README as instruções de compilação e uso, bem como gráficos* com o resultado de dois experimentos, um com a temperatura de referência fixa e outro seguindo uma curva pré-definida.
 
Obs: 
\* Serão necessários dois gráficos para cada experimento. Um deles plotando as temperaturas (Ambiente, Interna e Referência (Potenciômetro)) e outro gráfico com o valor do acionamento dos atuadores (Resistor / Ventoinha) em valor percentual entre -100% e 100%.

## 7. Comunicação UART com a ESP32

A comunicação com a ESP32 deve seguir o mesmo protocolo MODBUS utilizado no [Exercício 2](https://gitlab.com/fse_fga/exercicios/exercicio-2-uart-modbus).
 
A ESP32 será responsável por:
1. Efetuar a medição da temperatura interna (Sensor DS18B20);
2. Realizar a comunicação com o dashboard (Thingsboard) para definir:
   2.1 O valor da temperatura de referência;
   2.2 Os comandos de ligar e desligar o forno;
   2.3 Os comandos de inicar e parar o aquecimento;
   2.4 Os comandos para a mudança de modo de controle (Temperatura de Referência ou Curvas de Temperatura);
3. Atualizar informações sobre as temperaturas e o sinal de controle no dashboard (ThingsBoard).

Para acessar as informações via UART envie mensagens em formato MODBUS com o seguinte conteúdo:

1. Código do Dispositivo no barramento: 0x01 (Endereço da ESP32);  
2. Leitura do Valor de Temperatura Interna (TI): Código 0x23, Sub-código: 0xC1 + 4 últimos dígitos da matrícula. O retorno será o valor em Float (4 bytes) da temperatura interna do sistema com o pacote no formato MODBUS;
3. Leitura da temperatura de referência - TR (Potenciômetro): Código 0x23, Sub-código: 0xC2 + 4 últimos dígitos da matrícula. O retorno será o valor em Float (4 bytes) da temperatura de referência definida pelo usuário com o pacote no formato MODBUS;
4. Envio do sinal de controle (Resistor / Ventoinha): Código 0x16,  Sub-código: 0xD1 + 4 últimos dígitos da matrícula, Valor em Int (4 bytes).
5. Envio do sinal de referência nos casos em que o sistema esteja sendo controlado ou pelo terminal ou pela curva de referência: Código 0x16,  Sub-código: 0xD2 + 4 últimos dígitos da matrícula, Valor em Float (4 bytes).
6. Leitura dos Comandos de usuário: Código 0x23,  Sub-código: 0xC3;
7. Envio do estado interno do sistema em resposta aos comandos de usuário:
   1. Estado (Ligado / Desligado): Código 0x16,  Sub-código: 0xD3 + byte;
   2. Modo de Controle (Referência Fixa = 0 / Curva de Temperatura = 1): Código 0x16,  Sub-código: 0xD4 + byte;

<p style="text-align: center;">Tabela 1 - Códigos do Protocolo de Comunicação</p>

| Endereço da ESP32 | Código |	Sub-código + Matricula | Comando de Solicitação de Dados |	Mensagem de Retorno |
|:-:|:-:|:-:|:--|:--|
| **0x01** | **0x23** | **0xC1** N N N N |	Solicita Temperatura Interna  | 0x00 0x23 0xC1 + float (4 bytes) |
| **0x01** | **0x23** | **0xC2** N N N N |	Solicita Temperatura de Referência	| 0x00 0x23 0xC2 + float (4 bytes) |
| **0x01** | **0x23** | **0xC3** N N N N |	Lê comandos do usuário  | 0x00 0x23 0xC3 + int (4 bytes de comando) | 
| **0x01** | **0x16** | **0xD1** N N N N |	Envia sinal de controle Int (4 bytes) | 0x00 0x16 0xD1 |
| **0x01** | **0x16** | **0xD2** N N N N |	Envia sinal de Referência Float (4 bytes) | 0x00 0x16 0xD2 |
| **0x01** | **0x16** | **0xD3** N N N N |	Envia Estado do Sistema (Ligado = 1 / Desligado = 0) | 0x00 0x16 0xD3 + int (4 bytes de estado) | 
| **0x01** | **0x16** | **0xD4** N N N N |	Modo de Controle da Temperatura de referência (Dashboard = 0 / Curva/Terminal = 1) (1 byte) | 0x00 0x16 0xD4 + int (4 bytes de modo de controle) | 
| **0x01** | **0x16** | **0xD5** N N N N |	Envia Estado de Funcionamento (Funcionando = 1 / Parado = 0) | 0x00 0x16 0xD5 + int (4 bytes de estado) | 
| **0x01** | **0x16** | **0xD6** N N N N |	Envia Temperatura Ambiente (Float)) | 0x00 0x16 0xD6 + float (4 bytes) | 

**Obs 1**: todas as mensagens devem ser enviadas com o CRC e também recebidas verificando o CRC. Caso esta verificação não seja válida, a mensagem deverá ser descartada e uma nova solicitação deverá ser realizada.    
**Obs 2**: Os comandos D2 e D4 serão usados para o controle da temepratura de referência pelo software embarcado na Raspberry seja no modo 'Curva de Temperatura' ou no modo de 'Debug via Termnal' da aplicação. O 0xD4 determina qual a temperautra de referência que irá aparecer no dashboard se é a configurada pelo usuário nos botões do próprio dashboard ou a temperatura enviada pelo via UART. Já o 0xD2 é o subcomando para envio da temperatura de referência.

<p style="text-align: left;">Tabela 2 - Comandos de Usuário via UART</p>

| Comando | Código |
|:--|:-:|
| **Liga** o Forno | 0xA1 |
| **Desliga** o Forno | 0xA2 |
| **Inicia** aquecimento | 0xA3 |
| **Cancela** processo | 0xA4 |
| **Menu** : alterna entre o modo de Temperatura de Referência e Curva de Temperatura | 0xA5 |  

A leitura dos comandos via UART deve ser realizada a cada **500 ms**.

## 8. Parâmetros de PID

Para o uso do controle do PID, estão sendo sugeridos os seguintes valores para as constantes:

<!-- Placa Rasp42 com Fonte de 12V: -->
- **Kp** = 30.0
- **Ki** = 0.2
- **Kd** = 400.0
  
<!-- Placa Rasp43 com Fonte de 5V:
- **Kp** = 20.0
- **Ki** = 0.1
- **Kd** = 100.0 -->

Porém, vocês estão livres para testar outros valores que sejam mais adequados.

### Acionamento do Resistor 

O **resistor** deve ser acionado utilizando a técnica de PWM (sugestão de uso da biblioteca WiringPi / SoftPWM). A intensidade de acionamento do resistor por variar entre 0 e 100%.

### Acionamento da Ventoinha

A **venotinha** também deve ser acionada utilizando a técnica de PWM. Porém, há um limite inferior de 40% de intensidade para seu acionamento pelas características do motor elétrico. Ou seja, caso o valor de saída do PID esteja entre 0 e -40%, a ventoinha deverá ser acionada com 40% de sua capacidade.

Observa-se ainda que a saída negativa só indica que o atuador a ser acionado deve ser a ventoinha e não o resistor e o valor de PWM a ser definido deve ser positivo, invertendo o sinal.

## 9. Critérios de Avaliação

### Entrega:

1. Link do repositório incluindo o README com as instruções de execução (Para projetos em C/C++ é necessário incluir o Makefile);
2. Vídeo de até 5 min mostrando o sistema em funcionamento (Prioritariamente mostrando o funcionamento em si e se possível destacar partes do código fonte).

A avaliação será realizada seguindo os seguintes critérios:

|   ITEM    |   COMENTÁRIO  |   VALOR   |
|------------------------|---------------------------------------------------------------------------------------------------------|---------|
|**Implementação do controlador PID** | Correta implementação do controlador PID (Resistor / Venotinha), incluindo a leitura das temperaturas e acionamento dos atuadores. |    1,5 |
|**Implementação do controlador PID** | Correta implementação do controle de temperatura por referência fixa ou pela curva de temperatura em arquivo. |    1,0 |
|**Menu de controle (Dashboard)**        | Correta implementação da comunicação com o menu do dashboard. | 1,0 |
|**Menu de controle (Parâmtros)**        | Correta implementação do menu local (termnal) de parametrização do sistema (PID). | 0,5 |
|**Leitura da Temperatura Ambiente**| Leitura dos valores de Temperatura Ambiente (Sensor BME280). | 0,5 |
|**Comunicação UART** | Leitura dos valores de Temperatura Interna, Temperatura de Referência, Comandos e envio do sinal de controle através da comunicação MODBUS-UART. | 2,0 |
|**Armazenamento em arquivo**| Armazenamento em arquivo CSV dos dados medidos. |   0,5 |
|**Qualidade do Código**     | Utilização de boas práticas como o uso de bons nomes, modularização e organização em geral.    |  2,0 |
|**README com Experimento** | Documentação README com instruçoes de compilaçõa, uso e relatório do experimento com o gráfico. |  1,0 |
|**Pontuação Extra**         |   Qualidade e usabilidade acima da média.  |  0,5   |

## 10. Referências

[Controle Liga/Desliga - Wikipedia](https://pt.wikipedia.org/wiki/Controle_liga-desliga)  
[Controle PID - Wikipedia](https://pt.wikipedia.org/wiki/Controlador_proporcional_integral_derivativo)  
[Driver da Bosh para o sensor BME280](https://github.com/BoschSensortec/BME280_driver)  
[Biblioteca BCM2835 - GPIO](http://www.airspayce.com/mikem/bcm2835/)  
[Biblioteca WiringPi GPIO](http://wiringpi.com)  
[PWM via WiringPi](https://www.electronicwings.com/raspberry-pi/raspberry-pi-pwm-generation-using-python-and-c)

## 11. Execução e Organização

### Arquivos
Os arquivos que compõe a entrega são
```
main.c
UART.h
makefile
```
Os arquivos dentro da pasta PID e o arquivo BME280.h foram obtidos externamente

### Execução
Para executar o código é necessário apenas executar o comando
```
make run
```
Que é responsável pela criação da pasta exe e a compilação e execução do código.
<br>
<br>
Após a execução do código é possível ativar o forno, controlar a temperatura de referência e iniciar o aquecimento/resfriamento do sistema pela utilização da interface da ThingsBoard.
<br>
<br>
Enquanto o forno não estiver aquecendo/esfriando também é possível acessar o seguinte menu no terminal

![Figura](/figuras/MenuTerminal.png)

Que permite a alteração tanto dos valores de KP, KI e KD do PID, como também a definição manual dos valores de temperatura de referência, seja seguindo o padrão do arquivo curva_reflow.csv ou definição manual do usuário.
<br>
<br>
Por fim para fechar o sistema, caso o forno não esteja aquecendo/esfriando é possível encerrar o código pela opção Cancelar na ThingsBoard. 
Caso contrário é necessário inserir o comando CTRL+C no terminal do código. 