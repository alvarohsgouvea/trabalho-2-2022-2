#include "lcd.h"
#include "libs/BME280.h"
#include "libs/UART.h"
#include "libs/PID/pid.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <softPwm.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <signal.h>

// Thread
pthread_t PID_thread;
pthread_t menu_thread;

// Define some lcd device parameters
#define I2C_ADDR 0x27 

// GPIO
#define resistor 4
#define fan 5

// UART
int UART_CON;

// I2C
int i2c;

// Forno
int curve = 3;
int running = 0;
int start = 0;
int onOff = 0;
float temp_man = 0;
int lastResPWM = 0;
int lastFanPWM = 0;

// Basic Functions
void setup();
int init_uart();
void trataSinal(int sinal);
void *menu();
void *runPID();
void registerLog(float TI, float TR);



// Setup de pins + UART
void setup(){
    char buf[30];
    pid_configura_constantes(30.0, 0.2, 400.0);

    UART_CON = init_uart();

    if( wiringPiSetup() == -1){
        exit(1);
    }

    i2c = wiringPiI2CSetup(I2C_ADDR);

    pinMode(resistor, OUTPUT);
    pinMode(fan, OUTPUT);

    softPwmCreate(resistor, 0, 100);
    softPwmCreate(fan, 0, 100);

    //logFile
    FILE *logFile;
    if(access("log.csv", F_OK)){
        logFile = fopen("log.csv", "w");
        fprintf(logFile, "Date & Time; Temp Int; Temp Def; PWM Res; PWM Fan\n");
        fclose(logFile);
    }

    //Reseta pra curva da Dashboard
    int dash = 0;
    memcpy(buf, (char *)&dash, sizeof(dash));
    send(UART_CON, SEND_FUNCIONAMENTO, buf, 1);
    readData(UART_CON, buf, 9);
}

// Configurar UART
int init_uart(){
    int connection = -1;

    connection = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (connection == -1){
        printf("Erro na inicialização da UART\n");
    }else{
        printf("Conexão UART estabelecida\n");
    }

    return connection;
}

// logs
void registerLog(float TI, float TR){
    FILE *logFile;
    time_t rawtime;
    struct tm * timeinfo;
    char time_edited[250];

    logFile = fopen("log.csv", "a");
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );

        strcpy(time_edited, asctime(timeinfo));
        time_edited[strlen(time_edited)-1] = '\0';

        fprintf (logFile, "%s; %.2lf; %.2lf; %d%%; %d%%\n", time_edited, TI, TR, lastResPWM, lastFanPWM);
    fclose(logFile);
}

// Aquecer e esfriar 
void heating(float value){
    int sig = (int)value;
    char temp[5] = {};

    memcpy(temp, (char *)&sig, sizeof(sig));
    send(UART_CON, SEND_SIN_CTRL, temp, 4);
    
    lastResPWM = (int)value;
    if(lastResPWM > 50){
        lastFanPWM = 40;
        softPwmWrite(fan, 40);
    }
    softPwmWrite(resistor, value);
}

void cooling(float value){
    int sig = (int)value;
    value *= -1;
    char temp[5] = {};

    memcpy(temp, (char *)&sig, sizeof(sig));
    send(UART_CON, SEND_SIN_CTRL, temp, 4);
    if(value > 0 && value <= 40){
        value = 40;
    }

    lastFanPWM = (int)value;
    if(lastFanPWM > 40){
        lastResPWM = 0;
        softPwmWrite(resistor, 0);
    }
    softPwmWrite(fan, value);
}

//Fechar sistemas e conexoes
void trataSinal(int sinal){
    int valor = 0;
    char temp[10];

    memcpy(&temp, (char *)&valor, sizeof(valor));

    printf("SIGNAL: %d\n", sinal);
    softPwmWrite(resistor, 0);
    softPwmWrite(fan, 40);

    send(UART_CON, SEND_ESTADO_SISTEMA, temp, 1);
    readData(UART_CON, temp, 9);

    int dash = 0;
    memcpy(temp, (char *)&dash, sizeof(dash));
    send(UART_CON, SEND_FUNCIONAMENTO, temp, 1);
    readData(UART_CON, temp, 9);
    printf("Sistema Fechado\n");
    exit(0);
}

// Menu()
void temp_ref(){
    char buf[30];
    int dash = 1;
    int choice = 1;
        do{
        system("clear");
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n\n1. Curva de temperatura manual\n2. Curva Pre-Definida\n3. Curva pelo ThingsBoard\n>    ");
        scanf("%d", &choice);
        system("clear");

        switch(choice){
            case 1:
                printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\nInsira a temperatura que deseja atingir:     ");
                scanf("%f", &temp_man);
                memcpy(buf, (char *)&dash, 4);
                send(UART_CON, SEND_CNTRL_CURVA, buf, 4);

                memcpy(buf, (char *)&temp_man, 4);
                send(UART_CON, SEND_MD_REF, buf, 4);
                curve = 1;
            break;
            case 2:
                memcpy(buf, (char *)&dash, 4);
                send(UART_CON, SEND_CNTRL_CURVA, buf, 4);
                curve = 2;
            break;
            case 3:
                dash = 0;
                memcpy(buf, (char *)&dash, 4);
                send(UART_CON, SEND_CNTRL_CURVA, buf, 4);
                curve = 3;
            break;
        }
        system("clear");
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
        printf("Temperatura Redefinida!\nVoce sera redirecionado(a) ao menu!\n");
        sleep(2);
        menu();
    }while(choice < 1 || choice > 3);
}

void cmd_reader(){
    int cmd;
    char cmd_buf[45];


    printf("before menu\n");
    pthread_create(&menu_thread, NULL, menu, NULL);
    while(start){
        usleep(100000);
        request(UART_CON, USER_CMD_MD);
        readData(UART_CON, cmd_buf, 9);
        memcpy(&cmd, &cmd_buf[3], 1);

        switch(cmd){
            case 161:
                onOff = 1;
                memcpy(cmd_buf, (char *)&onOff, sizeof(onOff));
                send(UART_CON, SEND_ESTADO_SISTEMA, cmd_buf, 1);
                usleep(100000);
                readData(UART_CON, cmd_buf, 9);
            break;
            case 162:
                onOff = 0;
                memcpy(cmd_buf, (char *)&onOff, sizeof(onOff));
                send(UART_CON, SEND_ESTADO_SISTEMA, cmd_buf, 1);
                usleep(100000);
                readData(UART_CON, cmd_buf, 9);
            break;
            case 163:
                if(onOff == 1){
                    running = 1;
                    memcpy(cmd_buf, (char *)&running, sizeof(running));
                    send(UART_CON, SEND_FUNCIONAMENTO, cmd_buf, 1);
                    readData(UART_CON, cmd_buf, 9);
                    
                    printf("before pid\n");
                    pthread_create(&PID_thread, NULL, runPID, NULL);
                    pthread_join(PID_thread, NULL);

                    running = 0;

                    memcpy(cmd_buf, (char *)&running, sizeof(running));
                    send(UART_CON, SEND_FUNCIONAMENTO, cmd_buf, 1);
                    readData(UART_CON, cmd_buf, 9);
                }else{
                    printf("Por Favor Ligar o Forno Primeiro!\n");
                }
            break;
            case 164:
                onOff = 0;
                running = 0;

                //Cancelar thread e esperar novo input do user
                pthread_cancel(PID_thread);
                pthread_cancel(menu_thread);
                trataSinal(SIGINT);
            break;
        }
    }
}

void change_pid(){
        float KP = 30, KI = 0.2, KD = 400;
        system("clear");
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n\nInsira os Paramentros:\nKP:    ");
        scanf("%f", &KP);
        printf("KI:    ");
        scanf("%f", &KI);
        printf("KD:    ");
        scanf("%f", &KD);

        pid_configura_constantes(KP, KI, KD);
        system("clear");
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
        printf("Valores Redefinidos!\nVoce sera redirecionado(a) ao menu!\n");
        sleep(2);
        menu();
}

void *menu(){
    int choice = 1;

    do{
        system("clear");
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n\n1. Temperatura de Referencia\n2. Parametros PID\n>     ");
        scanf("%d", &choice);
        
        switch(choice){
            case 1:
                temp_ref();
                break;
            case 2:
                change_pid();
                break;
        }
    }while(choice < 1 || choice > 2);
}

void *runPID(){
    int flagPID = -1;
    double counter = 0;
    int running = 1, stable = 0, csvTemp, csvTime;
    float TI, TR;
    char temp[30], TI_temp[30];
    clock_t start, now;

    FILE *reflow;
    reflow = fopen("curva_reflow.csv", "r");
    fscanf(reflow, "%[^\n]s", temp);
    fscanf(reflow, "%d, %d\n", &csvTime, &csvTemp);

    start = clock();

    while(running != 0){
        // Visual Feedback
        now = clock();
        system("clear");
        // Temp Ref
        if(curve == 3){
            request(UART_CON, REQUEST_REF_TEMP);
            usleep(10000);
            readData(UART_CON, temp, 9);

            memcpy(&TR, &temp[3], 4);
        }else{
            if(csvTime <= (now-start)/CLOCKS_PER_SEC & curve == 2){
                temp_man = csvTemp;
                memcpy(&temp, (char *)&temp_man, 4);
                send(UART_CON, SEND_MD_REF, temp, 4);
                fscanf(reflow, "%d, %d\n", &csvTime, &csvTemp);
            }
            TR = temp_man;
        }

        request(UART_CON, REQUEST_INT_TEMP);
        usleep(10000);
        readData(UART_CON, TI_temp, 9);

        memcpy(&TI, &TI_temp[3], 4);


        pid_atualiza_referencia(TR);
        flagPID = pid_controle(TI);

        if(TR - TI <= 0.2 && TR - TI  >= -0.2 && stable == 5){
            running = 0;
        }else if(flagPID > 0){
            stable = 0;
            heating(flagPID);
        }else if(flagPID < 0){
            stable = 0;
            cooling(flagPID);
        }
        if(TR - TI <= 0.2 && TR - TI  >= -0.2){
            stable++;
        }

        registerLog(TI, TR);
        now = clock();
        printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n");
        printf("Temp Int: %.2lf°C   Temp Ref: %.2lf°C\n\nPWM Fan: %d%%    PWM Resistor: %d%%\n\n", TI, TR, lastFanPWM, lastResPWM);
        sleep(2);
    }
    softPwmWrite(fan, 0);
    softPwmWrite(resistor, 0);

    memcpy(temp, (char *)&running, sizeof(running));
    send(UART_CON, SEND_FUNCIONAMENTO, temp, 1);

    cmd_reader();
}

int main(){
    setup();
    signal(SIGINT, trataSinal);
    
    softPwmWrite(fan, 0);
    softPwmWrite(resistor, 0);

    start = 1;

    cmd_reader();

    close(UART_CON);
    return 0;
}